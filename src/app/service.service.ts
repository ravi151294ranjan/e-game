import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject, from} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private postUpdated = new Subject();

  constructor(private http: HttpClient) { }

  getPost() {
    this.http.get<{message: string , posts: any }>('http://localhost:3000/api/posts')
    .pipe(map((postData) => {
      return postData.posts.map(post => {
        return {
          title : post.title,
          content : post.content,
          id : post._id
        };
      });
    }))
    .subscribe((transformedPost) => {
      // this.posts = transformedPost;
      // this.postUpdated.next([...this.posts]);
    });
  }

  getPostUpdatedListner() {
    return this.postUpdated.asObservable();
  }

  addPost(title: string, content: string) {
    // tslint:disable-next-line:object-literal-shorthand
    const post = { id: null, title: title, content: content};
    this.http.post<{message: string, postId: string}>('http://localhost:3000/api/posts', post).
    subscribe((responseData) => {
      const id = responseData.postId;
      post.id = id;
      // this.posts.push(post);
      // this.postUpdated.next([...this.posts]);
    });
  }

  daletePost(postId: string) {
    this.http.delete('http://localhost:3000/api/posts/' + postId)
    .subscribe(() => {
      // const updatedPosts = this.posts.filter(post => post.id !== postId);
      // this.posts = updatedPosts;
      // this.postUpdated.next([...this.posts]);
    });
  }
}
